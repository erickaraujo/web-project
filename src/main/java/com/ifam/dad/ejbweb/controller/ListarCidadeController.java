package com.ifam.dad.ejbweb.controller;

import com.ifam.dad.ejbweb.dao.CidadeDAO;
import com.ifam.dad.ejbweb.modelo.Cidade;
import static com.ifam.dad.ejbweb.util.FaceUtils.redirect;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;
import javax.inject.Named;

//@ViewScoped
@Named(value="listarCidadeController")
public class ListarCidadeController {


    @Inject
    CidadeDAO cidadeDAO;

    private List<Cidade> listaCidade;

    private String descricaoCidade;

    @PostConstruct
    public void init(){
        listaCidade = cidadeDAO.list();
    }

    public List<Cidade> getListaCidades() {
        if (descricaoCidade == null) {
            listaCidade = cidadeDAO.list();
        }
        else{
            listaCidade = cidadeDAO.listPorNome(descricaoCidade);
        }
        return listaCidade;
    }

    public String getDescricaoCidade() {
        return descricaoCidade;
    }

    public void setDescricaoCidade(String descricaoCidade) {
        this.descricaoCidade = descricaoCidade;
    }
    
    public String cadastrar(){
        return redirect("index");
    }
}
