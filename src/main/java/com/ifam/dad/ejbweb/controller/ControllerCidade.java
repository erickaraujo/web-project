package com.ifam.dad.ejbweb.controller;

import com.ifam.dad.ejbweb.dao.CidadeDAO;
import com.ifam.dad.ejbweb.modelo.Cidade;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Named;

import static com.ifam.dad.ejbweb.util.FaceUtils.redirect;
import java.util.logging.Logger;
/**
 *
 * @author erick.araujo
 */
@Named(value ="controllerCidade")
@ViewScoped
public class ControllerCidade implements Serializable {
   
    
    private String nome;
    private String mensagem;
    
    Cidade cidade;
    
    @Inject
    private CidadeDAO dao;
    
    @PostConstruct
    private void init(){
//        System.out.println("-----------init------------");
//        System.out.println("init nome: " + nome);
//        System.out.println("init cidade: " + cidade);
//        System.out.println("init mensagem: " + mensagem);
////        if(cidade == null){
////            cidade = new Cidade();
////        }
    }
   
    public String cadastrar(){
        System.out.println("cad() getNome: " + getNome());
        System.out.println("cad() nome: " + nome);
        System.out.println("cad() cidade:" + cidade);
        mensagem = "--> nome: " + nome + " cidade: " + cidade;
        System.out.println("cad() mensagem: " + mensagem);
        
//        dao.create(cidade);
//        cidade = new Cidade();
        return redirect("index");
    }
         
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        System.out.println("setNome -> " + nome);
        this.nome = nome;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public String getMensagem() {
        return mensagem;
    }

}
