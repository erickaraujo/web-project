/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifam.dad.ejbweb.main;

import com.ifam.dad.ejbweb.dao.CidadeDAO;
import com.ifam.dad.ejbweb.modelo.Cidade;
import java.util.List;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author erick.araujo
 */


public class Main implements ServletContextListener{

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        populaTestes();
    }
    
    private void populaTestes(){
        
        try{
            CidadeDAO cidadeDao = getService(CidadeDAO.class);
            List<Cidade> res = cidadeDao.list();
            
            if(res.isEmpty()){
                Cidade cidade = new Cidade("Manaus");
                cidadeDao.update(cidade);
                System.out.println("cadastrado '" + cidade.getNome()+"'");
                
                Cidade cidade1 = new Cidade("Belém");
                cidadeDao.update(cidade1);
                System.out.println("cadastrado '" + cidade.getNome()+"'");
                
                Cidade cidade2 = new Cidade("Rio de Janeiro");
                cidadeDao.update(cidade2);
                System.out.println("cadastrado '" + cidade.getNome()+"'");
                
                Cidade cidade3 = new Cidade("São Paulo");
                cidadeDao.update(cidade3);
                System.out.println("cadastrado '" + cidade.getNome()+"'");                
                
            }
            
        }catch(Exception e){
            e.getMessage();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
  
    private <E> E getService(Class<E> clazz) throws NamingException {
        BeanManager beanManager;
        beanManager = (BeanManager) new InitialContext().lookup("java:comp/BeanManager");
        Bean<E> bean = (Bean<E>) beanManager.resolve(beanManager.getBeans(clazz));
        CreationalContext<E> creationalContext = beanManager.createCreationalContext(null);
        return bean.create(creationalContext);
    }
}
