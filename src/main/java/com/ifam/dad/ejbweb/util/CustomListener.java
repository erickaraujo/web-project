package com.ifam.dad.ejbweb.util;

import org.apache.log4j.Logger;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;


public class CustomListener implements javax.faces.event.PhaseListener {
    private Logger log = Logger.getLogger(CustomListener.class);

    public CustomListener() {
    }

    @Override
    public void afterPhase(PhaseEvent phaseEvent) {
        log.info("afterPhase: " + phaseEvent.getPhaseId());

    }

    @Override
    public void beforePhase(PhaseEvent phaseEvent) {
        log.info("beforePhase:" + phaseEvent.getPhaseId());
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.ANY_PHASE;
    }
}
