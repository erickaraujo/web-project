package com.ifam.dad.ejbweb.dao;

import com.ifam.dad.ejbweb.modelo.Cidade;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author erick.araujo
 */

@Stateless
public class CidadeDAO {
    
    @PersistenceContext
    private EntityManager em;
    private DAOGenerico<Cidade> dao;
    
    @PostConstruct
    private void init(){
        dao = new DAOGenerico<Cidade>(em, Cidade.class);
    }

    public void create(Cidade object) {
        dao.create(object);
    }

    public Cidade find(long id) {
        return dao.find(id);
    }

    @Named("lista_todas_cidades")
    public List<Cidade> list() {
        TypedQuery<Cidade> query = em.createQuery("select c from Cidade c order by c.nome", Cidade.class);
        return query.getResultList();
    }
    
    @Named("lista_nome_cidades")
    public List<Cidade> listPorNome(String nome){
        TypedQuery<Cidade> query = em.createQuery("select c from Cidade c where c.nome like :nome order by c.nome", Cidade.class);
        query.setParameter("nome", "%"+nome+"%");
        return query.getResultList();
    }

    public void remove(Cidade object) {
        dao.remove(object);
    }

    public Cidade update(Cidade object) {
        return dao.update(object);
    }
    
    
}
