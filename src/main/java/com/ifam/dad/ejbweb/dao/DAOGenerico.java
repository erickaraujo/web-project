/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifam.dad.ejbweb.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author erick.araujo
 */


public class DAOGenerico<E> {
    
    private final EntityManager em;
    private final Class<E> classe;
    
    
    public DAOGenerico(EntityManager em, Class<E> classe){
        this.em = em;
        this.classe = classe;
    }
    
    public void create(E object){
        em.persist(object);
    }
    
    public E find(long id){
        return em.find(classe, id);
    }
    
    public List<E> list(){
        return em.createQuery("select i from " + classe.getName() + " i", classe).getResultList();
    }
    
    public void remove(E object){
        em.remove(em.contains(object) ? object : em.merge(object));
    }
    
    public E update(E object){
        return em.merge(object);
    }
}
